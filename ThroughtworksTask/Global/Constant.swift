//
//  Constant.swift
//  ThroughtworksTask
//
//  Created by Techops on 20/12/19.
//  Copyright © 2019 Techops. All rights reserved.
//

import Foundation

let BaseURL = "https://www.mocky.io/"
var persistentArr = [ModelShopper]()


func storeToUserDefault(arr : [ModelShopper]){
    let defaults = UserDefaults.standard
    if let encoded = try? JSONEncoder().encode(arr) {
        UserDefaults.standard.set(encoded, forKey: "ShopperArr")
    }
    defaults.synchronize()
}

func retrieveFromUserDefault() -> [ModelShopper]{
    if let blogData = UserDefaults.standard.data(forKey: "ShopperArr"),
        let arrObj = try? JSONDecoder().decode([ModelShopper].self, from: blogData) {
        return arrObj
    }
    return [ModelShopper]()
}
