//
//  ApiClient.swift
//  ThroughtworksTask
//
//  Created by Techops on 20/12/19.
//  Copyright © 2019 Techops. All rights reserved.
//

import Foundation


class ApiClient{

    static let shared = ApiClient()
    
    func getShopperList(url: String, successHandler: @escaping ([ModelShopper]) -> Void, errorHandler: @escaping (Error?) -> Void) {
        let apiUrl = "\(BaseURL)\(url)"
        var request = URLRequest(url: URL(string: apiUrl)!)
               request.httpMethod = "GET"

               URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
                   do {
                      let jsonDecoder = JSONDecoder()
                       let responseModel = try jsonDecoder.decode([ModelShopper].self, from: data!)
                    successHandler(responseModel)
                   } catch {
                    errorHandler(error)
                       print("JSON Serialization error")
                   }
               }).resume()
    }
}
