//
//  String+Throughtworks.swift
//  ThroughtworksTask
//
//  Created by Techops on 21/12/19.
//  Copyright © 2019 Techops. All rights reserved.
//

import Foundation
import UIKit


extension String {
    func toDecimalWithAutoLocale() -> Decimal? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal

        //** US,CAD,GBP formatted
        formatter.locale = Locale(identifier: "en_US")

        if let number = formatter.number(from: self) {
            return number.decimalValue
        }

        //** EUR formatted
        formatter.locale = Locale(identifier: "de_DE")

        if let number = formatter.number(from: self) {
           return number.decimalValue
        }

        return nil
    }

    func toDoubleWithAutoLocale() -> Double? {
        let str = self.replacingOccurrences(of: "$", with: "")
        guard let decimal = str.toDecimalWithAutoLocale() else {
            return nil
        }
        return NSDecimalNumber(decimal:decimal).doubleValue
    }
}
